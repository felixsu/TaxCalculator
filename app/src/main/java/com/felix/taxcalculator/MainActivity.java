package com.felix.taxcalculator;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public int getQty(String qty){
        if (qty.equals("")){
            return 0;
        }else{
            return Integer.parseInt(qty);
        }
    }

    public int getSubTotal(String subtotal){
        if (subtotal.equals("")){
            return 0;
        }else{
            return Integer.parseInt(subtotal);
        }
    }

    public int getPrice(String price){
        if (price.equals("")){
            return 0;
        }else{
            return Integer.parseInt(price);
        }
    }

    public int getTax(String tax){
        if (tax.equals("")){
            return 0;
        }else{
            return Integer.parseInt(tax);
        }
    }

    public int getService(String service){
        if (service.equals("")){
            return 0;
        }else{
            return Integer.parseInt(service);
        }
    }

    public int calcTax(int price, int tax) {
        return price*tax/100;
    }

    public int calcService(int price, int service) {
        return price*service/100;
    }

    public void add_item(View view){
        EditText tPrice = (EditText)findViewById(R.id.price);
        TextView tQty = (TextView)findViewById(R.id.qty);
        TextView tSubTotal = (TextView)findViewById(R.id.subtotal);

        String mPrice = tPrice.getText().toString();
        String mQty = tQty.getText().toString();
        String mSubTotal = tSubTotal.getText().toString();

        int vPrice = getPrice(mPrice);
        int vQty = getQty(mQty);
        int vSubTotal = getSubTotal(mSubTotal);

        if(vPrice>0){
            vQty++;
            vSubTotal+=vPrice;
        }

        mQty = Integer.toString(vQty);
        mSubTotal = Integer.toString(vSubTotal);

        tPrice.setText("");
        tQty.setText(mQty);
        tSubTotal.setText(mSubTotal);
        hideSoftKeyboard(this);
    }
    public void app_finalize(View view){
        int vprice;
        int vtax;
        int vservice;
        String vtotal;

        TextView tPrice = (TextView)findViewById(R.id.subtotal);
        EditText tTax = (EditText) findViewById(R.id.tax);
        EditText tService = (EditText) findViewById(R.id.service);

        String mPrice = tPrice.getText().toString();
        String mTax = tTax.getText().toString();
        String mService = tService.getText().toString();

        vprice = getPrice(mPrice);
        vtax = calcTax(getTax(mTax), vprice);
        vservice = calcService(getService(mService), vprice + vtax);
        vtotal= Integer.toString(vprice+vtax+vservice);

        TextView textView = (TextView)findViewById(R.id.total);
        textView.setText(vtotal);
    }

    public void app_reset(View view){
        EditText tPrice = (EditText)findViewById(R.id.price);
        EditText tTax = (EditText)findViewById(R.id.tax);
        EditText tService = (EditText)findViewById(R.id.service);
        TextView tTotal = (TextView)findViewById(R.id.total);
        TextView tQty = (TextView)findViewById(R.id.qty);
        TextView tSubTotal = (TextView)findViewById(R.id.subtotal);


        tPrice.setText("");
        tQty.setText("0");
        tSubTotal.setText("0");
        tTax.setText("10");
        tService.setText("5");
        tTotal.setText("0");
    }

    public void add_tax(View view){
        EditText tTax = (EditText)findViewById(R.id.tax);
        String mTax = tTax.getText().toString();
        int vTax = getTax(mTax);
        vTax++;
        if(vTax > 25){
            vTax = 25;
        }
        mTax = Integer.toString(vTax);
        tTax.setText(mTax);
    }

    public void minus_tax(View view){
        EditText tTax = (EditText)findViewById(R.id.tax);
        String mTax = tTax.getText().toString();
        int vTax = getTax(mTax);
        vTax--;
        if(vTax<0){
            vTax = 0;
        }
        mTax = Integer.toString(vTax);
        tTax.setText(mTax);
    }

    public void add_service(View view){
        EditText tService = (EditText)findViewById(R.id.service);
        String mService = tService.getText().toString();
        int vService = getTax(mService);
        vService++;
        if(vService>25){
            vService=25;
        }
        mService = Integer.toString(vService);
        tService.setText(mService);
    }

    public void minus_service(View view){
        EditText tService = (EditText)findViewById(R.id.service);
        String mService = tService.getText().toString();
        int vService = getTax(mService);
        vService--;
        if(vService<0){
            vService=0;
        }
        mService = Integer.toString(vService);
        tService.setText(mService);
    }

    public void testTax(View view){

    }
}
